#!/usr/bin/env
import boto3
import json
import subprocess


def lambda_function(event, context):
    client = boto3.client('apigateway')
    client2 = boto3.client('wafv2')

    response = client2.get_ip_set(
        Name='waf_uat_ipset',
        Scope='CLOUDFRONT',
        Id=''
    )

    results = response["IPSet"]["Addresses"]

    restrict_policy = """{
        "Version": "2012-10-17",
        "Statement": [{
            "Effect": "Allow",
            "Principal": "*",
            "Action": "execute-api:Invoke",
            "Resource": "execute-api:/*/*/*"
        },
            {
            "Effect": "Deny",
                "Principal": "*",
                "Action": "execute-api:Invoke",
                "Resource": "execute-api:/*/*/*",
                "Condition": {
                    "NotIpAddress": {
                        "aws:SourceIp": "restricted_ips"
                    }
                }
        }
        ]
    }"""

    restricted_ips_policy = restrict_policy.replace(
        '"restricted_ips"', json.dumps(results))

    restrict_policy_format = restricted_ips_policy.replace('"', '\\"')
    policy = restrict_policy_format
    print(policy)

    response = client.update_rest_api(
        restApiId='Id',
        patchOperations=[
            {
                'op': 'replace',
                'path': "/policy",
                'value': policy
            }
        ]
    )
    return("SUCCES!!")
